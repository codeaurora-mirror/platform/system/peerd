# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

# Common variables
# ========================================================

peerdCommonCppExtension := .cc
peerdCommonCFlags := \
	-Wall \
	-Werror \
	-Wno-missing-field-initializers \
	-Wno-unused-parameter \

peerdCommonCppFlags := \
	-Wno-sign-compare \
	-Wno-sign-promo \
	-Wno-non-virtual-dtor \

peerdCommonCIncludes := \
	$(LOCAL_PATH)/.. \
	external/gtest/include \

peerdSharedLibraries := \
	libavahi-client \
	libavahi-common \
	libchrome \
	libchrome-dbus \
	libchromeos \
	libchromeos-dbus \
	libdbus \

# peerd-common
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := peerd-common
LOCAL_C_INCLUDES := $(peerdCommonCIncludes)
LOCAL_CFLAGS := $(peerdCommonCFlags)
LOCAL_CLANG := true
LOCAL_CPP_EXTENSION := $(peerdCommonCppExtension)
LOCAL_CPPFLAGS := $(peerdCommonCppFlags)
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)
LOCAL_RTTI_FLAG := -frtti
LOCAL_SHARED_LIBRARIES := $(peerdSharedLibraries)
LOCAL_STATIC_LIBRARIES :=

LOCAL_SRC_FILES := \
	avahi_client.cc \
	avahi_service_discoverer.cc \
	avahi_service_publisher.cc \
	constants.cc \
	dbus_constants.cc \
	dbus_bindings/dbus-service-config.json \
	dbus_bindings/org.chromium.peerd.Manager.dbus-xml \
	dbus_bindings/org.chromium.peerd.Peer.dbus-xml \
	dbus_bindings/org.chromium.peerd.Service.dbus-xml \
	discovered_peer.cc \
	manager.cc \
	peer.cc \
	peer_manager_impl.cc \
	published_peer.cc \
	service.cc \
	technologies.cc \
	typedefs.cc \

include $(BUILD_STATIC_LIBRARY)

# peerd
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := peerd
LOCAL_C_INCLUDES := $(peerdCommonCIncludes)
LOCAL_CFLAGS := $(peerdCommonCFlags)
LOCAL_CLANG := true
LOCAL_CPP_EXTENSION := $(peerdCommonCppExtension)
LOCAL_CPPFLAGS := $(peerdCommonCppFlags)
LOCAL_RTTI_FLAG := -frtti
LOCAL_REQUIRED_MODULES := \
	init.peerd.rc \
	org.chromium.peerd.conf \

LOCAL_SHARED_LIBRARIES := $(peerdSharedLibraries)
LOCAL_WHOLE_STATIC_LIBRARIES := peerd-common

LOCAL_SRC_FILES := \
	main.cc

include $(BUILD_EXECUTABLE)

ifdef INITRC_TEMPLATE
include $(CLEAR_VARS)
LOCAL_MODULE := init.peerd.rc
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/$(TARGET_COPY_OUT_INITRCD)

include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE): $(INITRC_TEMPLATE)
	$(call generate-initrc-file,peerd,,)
endif

# libpeerd-client
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := libpeerd-client
LOCAL_DBUS_PROXY_PREFIX := peerd

LOCAL_SRC_FILES := \
	dbus_bindings/dbus-service-config.json \
	dbus_bindings/org.chromium.peerd.Manager.dbus-xml \
	dbus_bindings/org.chromium.peerd.Peer.dbus-xml \
	dbus_bindings/org.chromium.peerd.Service.dbus-xml \

include $(BUILD_SHARED_LIBRARY)

# peerd_testrunner
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := peerd_testrunner
LOCAL_C_INCLUDES := \
	$(peerdCommonCIncludes) \
	external/gmock/include \

LOCAL_CFLAGS := $(peerdCommonCFlags)
LOCAL_CPP_EXTENSION := $(peerdCommonCppExtension)
LOCAL_CPPFLAGS := $(peerdCommonCppFlags)

LOCAL_SHARED_LIBRARIES := \
	$(peerdSharedLibraries) \

LOCAL_STATIC_LIBRARIES := \
	peerd-common \
	libchrome_dbus_test_helpers \
	libchrome_test_helpers \
	libchromeos-test-helpers \
	libgtest \
	libgmock \
	libweave-test \

LOCAL_RTTI_FLAG := -frtti
LOCAL_CLANG := true

LOCAL_SRC_FILES := \
	avahi_client_unittest.cc \
	avahi_service_publisher_unittest.cc \
	discovered_peer_unittest.cc \
	manager_unittest.cc \
	peer_manager_impl_unittest.cc \
	peer_unittest.cc \
	published_peer_unittest.cc \
	peerd_testrunner.cc \
	service_unittest.cc \
	test_util.cc \

include $(BUILD_NATIVE_TEST)

# DBus config files for /etc/dbus-1
# ========================================================
include $(CLEAR_VARS)
LOCAL_MODULE := org.chromium.peerd.conf
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/dbus-1
LOCAL_SRC_FILES := dbus/org.chromium.peerd.conf
include $(BUILD_PREBUILT)
